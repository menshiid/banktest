﻿using Bank.Core;
using Bank.Core.DTO;
using Banks;
using MSHTML;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace abchina
{
    public class AbcchinaModel : BankModel
    {
        public override string BankName => "中国农业银行";

        public override string LoginURL => "https://perbank.abchina.com/EbankSite/startup.do";

        public override string BankID => "ABCHINA";

        public override int BankCode => 103;

        /// <summary>
        /// 目前的交易紀錄
        /// </summary>
        private List<TranscationData> CurrentTranscationData = new List<TranscationData>();


        /// <summary>
        /// 目前狀態
        /// </summary>
        private Status CurrentStatus = Status.Idle;

        /// <summary>
        /// 狀態
        /// </summary>
        private enum Status
        {
            /// <summary>
            /// 停止中
            /// </summary>
            Idle,
            /// <summary>
            /// 開始取得交易紀錄
            /// </summary>
            StartGetTranscation,
            /// <summary>
            /// 帳號清單
            /// </summary>
            AccountList,
            /// <summary>
            /// 已取得帳號清單
            /// </summary>
            OnAccountListComplete,
            /// <summary>
            /// 開始查詢
            /// </summary>
            StartQuery,
            /// <summary>
            /// 取得交易紀錄
            /// </summary>
            GetTranscation,
            /// <summary>
            /// 取得交易詳細記錄
            /// </summary>
            GetTranscationDetail,
            /// <summary>
            /// 取得批次處理詳細記錄
            /// </summary>
            GetBatchDetail,
            /// <summary>
            /// 登入
            /// </summary>
            Login
        }

        /// <summary>
        /// 目前模式
        /// </summary>
        private Modes CurrentMode = Modes.GetHistory;

        /// <summary>
        /// 模式
        /// </summary>
        private enum Modes
        {
            /// <summary>
            /// 擷取歷史資料
            /// </summary>
            GetHistory,
            /// <summary>
            /// 監控新資料
            /// </summary>
            WatchNewData,
            /// <summary>
            /// 等待中
            /// </summary>
            Waiting
        }

        /// <summary>
        /// 定時鬧鐘
        /// </summary>
        private Alarm alarm;

        private int count = 1;

        private Dictionary<string, Expression<Func<TranscationData, object>>> TranscationDataMapper = new Dictionary<string, Expression<Func<TranscationData, object>>>()
        {
            {"付款方-户名", t=> t.AccountName },{"付款方-账号", t=> t.PayAccount },
            {"收款方-户名", t=> t.RecieverAccountEmailPhone },{"收款方-账号", t=> t.RecieverAccountName },
            {"金额（小写）", t=> t.Money }, {"币种", t=> t.BookinCurrency},
            {"交易类型", t=> t.Desc }, {"交易渠道", t=> t.TransectionWay },
            {"交易行名", t=> t.TransectionFiled }, {"交易摘要", t=> t.Comment },
            {"交易用途", t=> t.TransectionStatus }
        };

        private void GotoWaiting()
        {
            ViewManager.Instance.HideLoading();
            CurrentMode = Modes.Waiting;
            alarm?.Stop();
            alarm = new Alarm();
            alarm.Interval = Config.Instance.RefreshInterval;
            alarm.OnAlarm += Alarm_OnAlarm;
            alarm.Start();
        }

        /// <summary>
        /// 鬧鐘時間到
        /// </summary>
        private void Alarm_OnAlarm()
        {
            ViewManager.Instance.ShowLoadiing();
            CurrentMode = Modes.WatchNewData;
            CurrentStatus = Status.StartGetTranscation;
            Run(() =>
            {
                //按下查詢按鈕
                GotoQuery(DateTime.Today);
                ViewManager.Instance.UpdateCounter($"{count++}");
                Run(() =>
                {
                    GetTranscation();
                }, 5000);
            });
        }

        public override void OnSetupAccount(AccountDTO Account)
        {
            base.OnSetupAccount(Account);

            CurrentStatus = Status.Login;

            Run(() =>
            {
                IE.RunJS("$('.m-uesrtxtbox').append('<div>密码：" + Account.password + "</div>');");
                var UserName = IE.FindInputByName("username").FirstOrDefault();
                if (UserName != null)
                {
                    UserName.value = Account.account;
                }
            });
        }

        /// <summary>
        /// 將交易記錄存入資料庫
        /// </summary>
        /// <param name="data">交易記錄</param>
        private void SaveToDatabase(IEnumerable<TranscationData> data)
        {
            try
            {
                //File.WriteAllText("test.json", JsonConvert.SerializeObject(data));
                using (Database db = new Database())
                {
                    var map = db.GetAccountsByAccount(data.Select(x => x.TargetAccount))
                                .ToDictionary(x => x.account, x => x.id);
                    db.AddTranscations(data.Where(x => !x.IsSaved)
                                           .Select(x =>
                                           {
                                               int id = -1;
                                               if (!string.IsNullOrEmpty(x.TargetAccount))
                                               {
                                                   map.TryGetValue(x.TargetAccount, out id);
                                               }
                                               var dto = new TranscationDTO()
                                               {
                                                   sAccountId = SourceAccount.id,
                                                   tAccountId = id,
                                                   money = x.Money.ToString(),
                                                   balance = x.Balance,
                                                   desc = x.Desc,
                                                   currency = "RMB",
                                                   inOut = x.Money > 0 ? 1 : 2,
                                                   tranTime = x.Date,
                                                   AccountName = x.AccountName,
                                                   PayAccount = x.PayAccount,
                                                   OpeningAccountbank = x.OpeningAccountbank,
                                                   TransectionWay = x.TransectionWay,
                                                   RecieverAccountName = x.RecieverAccountName,
                                                   RecieverAccountEmailPhone = x.RecieverAccountEmailPhone,
                                                   RecieverOpeningAccountbank = x.RecieverOpeningAccountbank,
                                                   TransectionStatus = x.TransectionStatus,
                                                   Comment = x.Comment,
                                                   MoneyExchangeLogo = x.MoneyExchangeLogo,
                                                   CardNo = x.CardNo,
                                                   AccountingDate = x.AccountingDate,
                                                   TransectionFiled = x.TransectionFiled,
                                                   TransectionFiledShortName = x.TransectionFiledShortName,
                                                   TargetAccountName = x.TargetAccountName,
                                                   TransectionThrough = x.TransectionThrough,
                                                   RestAmount = x.RestAmount,
                                                   TransectionAmount = x.TransectionAmount,
                                                   BookinCurrency = x.BookinCurrency,
                                                   BookingAmount = x.BookingAmount
                                               };
                                               return dto;
                                           }));
                }
                foreach (var d in data)
                {
                    d.IsSaved = true;
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Error(e);
            }
        }

        /// <summary>
        /// 取得交易
        /// </summary>
        public override void OnGetTranscations()
        {
            this.CurrentMode = !SourceAccount.isFetch ? Modes.GetHistory : Modes.WatchNewData;
            CurrentStatus = Status.StartGetTranscation;
            Run(() => IE.RunJS("$(\"a:contains('明细')\", $('#contentFrame').contents())[0].click()"));
            ViewManager.Instance.ShowLoadiing();
        }


        private void SetMethod<T, R>(Expression<Func<T, R>> expression, T target, R value)
        {
            if (expression.Body is MemberExpression)
            {
                var memberExpression = (MemberExpression)expression.Body;
                var property = (PropertyInfo)memberExpression.Member;
                var setMethod = property.GetSetMethod();
                setMethod?.Invoke(target, new object[] { Convert.ChangeType(value, property.PropertyType) });
            }
            else
            {
                var op = ((UnaryExpression)expression.Body).Operand;
                string name = ((MemberExpression)op).Member.Name;
                var property = target.GetType().GetProperty(name);
                var setMethod = property.GetSetMethod();
                setMethod?.Invoke(target, new object[] { Convert.ChangeType(value, property.PropertyType) });
            }

        }

        /// <summary>
        /// 取得目前資料在UI上的index
        /// </summary>
        /// <param name="data">UI資料來源</param>
        /// <param name="trans">比對資料來源</param>
        /// <param name="srcIndex">比對來源index</param>
        /// <returns>UI上的index</returns>
        private int GetIndexFromUI(List<TranscationData> data, List<TranscationData> trans, int srcIndex)
        {
            var HashCode = trans[srcIndex].HashCode;
            var matches = data.Select((x, i) => new { src = x, code = x.HashCode, index = i })
                                 .Where(x => x.code == HashCode)
                                 .ToList();
            var index = -1;
            if (matches.Count == 1)
            {
                index = 0;
            }
            else if (matches.Count > 1)
            {
                var count = trans.Select((x, i) => new { src = x, code = x.HashCode, index = i })
                              .Where(x => x.code == HashCode && x.index >= srcIndex)
                              .Count();
                index = matches.Count - count;
            }
            if (index >= 0)
            {
                return matches[index].index;
            }
            else
            {
                return index;
            }
        }

        private void RunJS(string js, Action callback, int delay)
        {
            Run(() =>
            {
                IE.RunJS(js);
                Run(() =>
                {
                    callback();
                }, delay);
            });
        }

        private Task GotoCurrentDetail(List<TranscationData> data, int index)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            //var index = GetIndexFromUI(list, data, sindex);
            var js = "$('a.Receipt',$('#tradeDetailShowDiv tr', $('#contentFrame').contents()))[" + index + "].click()";
            RunJS(js, () => GetDetail(data, index, () =>
           {
               tcs.SetResult(true);
           }), 3000);
            return tcs.Task;
        }

        public void GotoQuery()
        {
            DateTime end = DateTime.Now;
            DateTime start = end.AddYears(-1);
            GotoQuery(start, end);
            //GotoQuery(DateTime.Parse("2018-03-15"), DateTime.Today);
        }

        public void GotoQuery(DateTime start, int days = 1)
        {
            DateTime end = start.AddDays(days);
            GotoQuery(start, end);
        }

        public void GotoQuery(DateTime start, DateTime end)
        {
            IE.RunJS("$(document).ready(function(){ setTimeout(function(){$('#startDate', $('#contentFrame').contents()).val('" + start.ToString("yyyy-MM-dd") + "');$('#endDate', $('#contentFrame').contents()).val('" + end.ToString("yyyy-MM-dd") + "');$('#btn_query', $('#contentFrame').contents())[0].click();}, 3000)})");//需設定日期
        }

        public Task GotoQuery(DateTime start)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            var end = start.AddYears(1);
            IE.RunJS("$(document).ready(function(){ setTimeout(function(){$('#startDate', $('#contentFrame').contents()).val('" + start.ToString("yyyy-MM-dd") + "');$('#endDate', $('#contentFrame').contents()).val('" + DateTime.Now.ToString("yyyy-MM-dd") + "');$('#btn_query', $('#contentFrame').contents())[0].click();}, 3000)})");//需設定日期
            Run(() => tcs.SetResult(true), 5000);
            return tcs.Task;
        }

        public Task GotoNext()
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            IE.RunJS("$('#nextPage', $('#contentFrame').contents())[0].click();");
            Run(() => tcs.SetResult(true), 3000);
            return tcs.Task;
        }

        private void GetTranscation()
        {
            Run(async () =>
            {
                if (CurrentMode == Modes.GetHistory)
                {
                    var data = await GetCurrentTranscationTable();
                    while (true)
                    {
                        for (int i = 0; i < data.Item1.Count; i++)
                        {
                            await GotoCurrentDetail(data.Item1, i);
                        }
                        CurrentTranscationData.AddRange(data.Item1);
                        if (data.Item2)
                        {
                            Debug.WriteLine("exit from GetHistory Count=" + CurrentTranscationData.Count);
                            break;
                        }
                        await GotoNext();
                        data = await GetCurrentTranscationTable();
                    }
                    if (!CurrentTranscationData.All(x => x.IsSaved))
                    {
                        SaveToDatabase(CurrentTranscationData);
                    }
                    GotoWaiting();
                }
                else if (CurrentMode == Modes.WatchNewData)
                {
                    var data = await GetCurrentTranscationTable();
                    //目前交易紀錄如果是空的話，從資料庫取出
                    if (CurrentTranscationData.Count == 0)
                    {
                        using (AbchinaDatabase db = new AbchinaDatabase())
                        {
                            //從資料庫讀取交易紀錄
                            var transData = db.GetLastTranscations(SourceAccount.id).ToList();
                            if (transData.Count == 0)
                            {
                                //資料庫沒資料的話直接開始取得紀錄
                                CurrentMode = Modes.GetHistory;
                                GetTranscation();
                            }
                            else
                            {
                                //有資料的話進行比對
                                CurrentTranscationData = transData.Select(x => x.ToTranscationData()).ToList();
                                var last = CurrentTranscationData.Last();
                                await GotoQuery(last.Date);

                                CompareTranscationData();
                            }
                        }
                    }
                    else
                    {
                        //有資料的話進行比對
                        var last = CurrentTranscationData.Last();
                        await GotoQuery(last.Date);

                        CompareTranscationData();
                    }
                }
            }, 5000);
        }

        private async void CompareTranscationData()
        {
            var current = CurrentTranscationData;
            var last = current.Last();
            var hash = last.HashCode;

            var data = await GetCurrentTranscationTable();
            bool finial = data.Item2;
            List<TranscationData> ui = data.Item1;
            var match = ui.Select((x, i) => new { h = x, i = i })
                          .Where(x => x.h.HashCode == hash)
                          .OrderByDescending(x => x.i)
                          .FirstOrDefault();
            if (match != null)
            {
                int i = match.i + 1;
                do
                {
                    for (; i < ui.Count; i++)
                    {
                        await GotoCurrentDetail(ui, i);
                    }
                    var newRows = ui.Skip(match.i + 1);
                    if (newRows.Any())
                    {
                        CurrentTranscationData.AddRange(newRows);
                    }
                    if (finial)
                    {
                        break;
                    }
                    await GotoNext();                   
                    data = await GetCurrentTranscationTable();
                    ui = data.Item1;
                    finial = data.Item2;
                    i = 0;
                } while (true);
                if (!CurrentTranscationData.All(x => x.IsSaved))
                {
                    SaveToDatabase(CurrentTranscationData);
                }
                GotoWaiting();
            }
            else
            {
                if (finial)
                {
                    SaveToDatabase(CurrentTranscationData);
                    GotoWaiting();
                    return;
                }
                else
                {
                    await GotoNext();
                    CompareTranscationData();
                }
            }

        }

        private void GetDetail(List<TranscationData> datalist, int sindex, Action callback)
        {
            //將主交易區從iframe內複製到最外層
            IE.RunJS("$('#data').remove();var data = $('<div id=\"data\"></div>');data.append($('#printdata',$('#contentFrame').contents()).parent().html());$(document.body).append(data);");
            Run(() =>
            {
                try
                {
                    var table1 = IE.FindInputByID("printdata");
                    var table2 = table1.FindElemetByTagName("table").FirstOrDefault();
                    var table = table2.FindElemetByTagName("tbody").FirstOrDefault();

                    var data = datalist[sindex];

                    Func<IEnumerable<IHTMLElement>, IHTMLElement, string> getValue = (tds, td) =>
                    {
                        var tdlist = tds.ToList();
                        for (var i = 0; i < tdlist.Count - 1; i++)
                        {
                            if (tdlist[i] == td)
                            {
                                return tdlist[i + 1].innerText?.Replace("&nbsp;", " ")?.Trim();
                            }
                        }

                        return null;
                    };

                    var trs = table.FindElemetByTagName("tr").ToList();
                    for (int i = 0; i < trs.Count; i++)
                    {
                        var tr = trs[i];
                        var tds = tr.FindElemetByTagName("td").ToList();
                        if (tds.Count == 1)
                        {
                            //交易时间
                            var span = tds.First().FindElemetByTagName("span").First();
                            var content = span.innerText.Split('：');
                            var key = content[0];
                            if (TranscationDataMapper.ContainsKey(key))
                            {
                                if (key == "交易时间")
                                {
                                    data.Date = DateTime.Parse(content[1]);
                                }
                                else
                                {
                                    SetMethod(TranscationDataMapper[content[0]], data, content[1]);
                                }
                            }
                        }
                        else if (tr.innerText.Contains("账号"))
                        {
                            SetMethod(TranscationDataMapper["付款方-账号"], data, tds[2].innerText);
                            SetMethod(TranscationDataMapper["收款方-账号"], data, tds[5].innerText);
                        }
                        else if (tr.innerText.Contains("户名"))
                        {
                            SetMethod(TranscationDataMapper["付款方-户名"], data, tds[1].innerText);
                            SetMethod(TranscationDataMapper["收款方-户名"], data, tds[3].innerText);
                        }
                        else
                        {
                            foreach (var td in tds)
                            {
                                var titlestr = td.innerText;
                                if (!string.IsNullOrEmpty(titlestr))
                                {
                                    foreach (var title in TranscationDataMapper.Keys)
                                    {
                                        if (titlestr.Contains(title))
                                        {
                                            SetMethod(TranscationDataMapper[title], data, getValue(tds, td));
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }

                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.ToString());
                }

                CurrentStatus = Status.GetTranscation;

                //回上一頁
                IE.RunJS("$('#btn_Cancel', $('#contentFrame').contents())[0].click()");

                callback();
            }, 3000);
        }

        /// <summary>
        /// IE的Callback 文件下載完成
        /// </summary>
        /// <param name="url"></param>
        protected override void OnDocumentComplete(string url)
        {
            if (url.Contains("index.do") && CurrentStatus == Status.Login)
            {
                OnGetTranscations();
            }
            else if (url.Contains("AccountTradeDetailQueryInitAct.do") && CurrentStatus == Status.StartGetTranscation)
            {
                Run(() =>
                {
                    CurrentStatus = Status.GetTranscation;
                    GotoQuery();
                    Run(() => GetTranscation(), 3000);
                });
            }
        }

        private Task<Tuple<List<TranscationData>, bool>> GetCurrentTranscationTable()
        {
            TaskCompletionSource<Tuple<List<TranscationData>, bool>> tcs = new TaskCompletionSource<Tuple<List<TranscationData>, bool>>();
            GetCurrentTranscationTable((data, final) => tcs.SetResult(Tuple.Create(data, final)));
            return tcs.Task;
        }

        /// <summary>
        /// 從IE畫面上取得目前的交易紀錄(清單)
        /// </summary>
        /// <param name="Callback"></param>
        private void GetCurrentTranscationTable(Action<List<TranscationData>, bool> Callback)
        {
            Run(() =>
            {
                //將交易紀錄清單從iframe內複製到最外層
                IE.RunJS("$('#data').remove();var data = $('<div id=\"data\"></div>');data.append($('#tradeDetailTable',$('#contentFrame').contents()).parent().html());$(document.body).append(data);");
                Run(() =>
                {
                    var table = IE.FindInputByID("AccountTradeDetailTable");
                    var trs = table.FindElemetByTagName("tr");

                    List<TranscationData> data = new List<TranscationData>();
                    int i = 0;
                    foreach (var tr in trs)
                    {
                        var tds = tr.FindElemetByTagName("td").ToList();
                        if (tds.Count > 0)
                        {
                            var d = new TranscationData()
                            {
                                Date = DateTime.Parse(tds[0].innerText),
                                Money = decimal.Parse(tds[1].innerText.Replace("+", "")),
                                Balance = decimal.Parse(tds[2].innerText),
                                Desc = tds[6].innerText,
                                RowIndex = i++
                            };
                            data.Add(d);
                        }
                    }
                    var nextPage = IE.FindInputByID("nextPage");
                    var style = nextPage.style;
                    bool finial = false;
                    if (style != null && style.cssText != null && style.cssText.Contains("none"))
                    {
                        finial = true;
                    }
                    var pageIndex = IE.FindInputByID("pageIndex");
                    if (pageIndex != null)
                    {
                        Debug.WriteLine("pageIndex = " + pageIndex.innerText);
                    }
                    Callback(data, finial);
                }, 2000);
            });
        }

    }
}
