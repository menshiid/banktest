﻿using Bank.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace abchina
{
    /// <summary>
    /// 交易紀錄
    /// </summary>
    class TranscationData
    {
        /// <summary>
        /// 日期
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// 描述
        /// </summary>
        public string Desc { get; set; }
        /// <summary>
        /// 金額
        /// </summary>
        public decimal Money { get; set; }
        /// <summary>
        /// 餘額
        /// </summary>
        public decimal Balance { get; set; }

        /// <summary>
        /// 來源帳號
        /// </summary>
        public string SourceAccount { get; set; }
        /// <summary>
        /// 目的帳號
        /// </summary>
        public string TargetAccount { get; set; }

        //2018.06.24 新增
        public string AccountName { get; set; }
        public string PayAccount { get; set; }
        public string OpeningAccountbank { get; set; }
        public string TransectionWay { get; set; }
        public string RecieverAccountName { get; set; }
        public string RecieverAccountEmailPhone { get; set; }
        public string RecieverOpeningAccountbank { get; set; }
        public string TransectionStatus { get; set; }
        public string Comment { get; set; }
        public string MoneyExchangeLogo { get; set; }
        public string CardNo { get; set; } //卡號
        public string AccountingDate { get; set; } //記帳日期
        public string TransectionFiled { get; set; } //交易場所
        public string TransectionFiledShortName { get; set; } //交易國家或地區簡稱
        public string TargetAccountName { get; set; } //對方戶名
        public string TransectionThrough { get; set; } //資金流向
        public string RestAmount { get; set; } //餘額
        public string TransectionAmount { get; set; } //交易金額
        public string BookinCurrency { get; set; } //記帳幣種
        public string BookingAmount { get; set; } //記帳金額

        public int RowIndex { get; set; } //對應螢幕上的index

        /// <summary>
        /// 是否已存入資料庫
        /// </summary>
        public bool IsSaved { get; set; } = false;

        public override string ToString()
        {
            return $"[{Date.ToString("yyyy-MM-dd HH:mm:ss")}] {Desc} " + (IsSaved ? "-Saved" : "");
        }

        public string HashCode
        {
            get
            {
                return $"{Date.ToString("yyyy-MM-dd HH:mm:ss")}{Money}{Balance}";
            }
        }
    }

    /// <summary>
    /// 交易物件的擴充物件
    /// </summary>
    internal static class TranscationDataExtension
    {
        /// <summary>
        /// 取得交易的唯一ID
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string GetEventID(this TranscationDTO data)
        {
            return data.eventId.Substring(1);
        }

        /// <summary>
        /// 從DB物件轉換成交易資料
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static TranscationData ToTranscationData(this TranscationDTO data)
        {
            return new TranscationData()
            {
                Balance = data.balance,
                Date = data.tranTime,
                Desc = data.desc,
                Money = decimal.Parse(data.money),
                IsSaved = true,
                AccountName = data.AccountName,
                PayAccount = data.PayAccount,
                OpeningAccountbank = data.OpeningAccountbank,
                TransectionWay = data.TransectionWay,
                RecieverAccountName = data.RecieverAccountName,
                RecieverAccountEmailPhone = data.RecieverAccountEmailPhone,
                RecieverOpeningAccountbank = data.RecieverOpeningAccountbank,
                TransectionStatus = data.TransectionStatus,
                Comment = data.Comment,
                MoneyExchangeLogo = data.MoneyExchangeLogo,
                CardNo = data.CardNo,
                AccountingDate = data.AccountingDate,
                TransectionFiled = data.TransectionFiled,
                TransectionFiledShortName = data.TransectionFiledShortName,
                TargetAccountName = data.TargetAccountName,
                TransectionThrough = data.TransectionThrough,
                RestAmount = data.RestAmount,
                TransectionAmount = data.TransectionAmount,
                BookinCurrency = data.BookinCurrency,
                BookingAmount = data.BookingAmount
            };
        }
    }
}
