﻿using Bank.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Bank.Core.DTO;

namespace abchina
{
    public class AbchinaDatabase : Database
    {
        /// <summary>
        /// 取得最終幾筆的交易紀錄
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IEnumerable<TranscationDTO> GetLastTranscations(int ID)
        {
            var first = connection.Query<TranscationDTO>("SELECT * FROM transferdetail WHERE sAccountId = @id ORDER BY id DESC LIMIT 1", new { id = ID }).FirstOrDefault();
            if (first == null)
            {
                return new List<TranscationDTO>();
            }
            return connection.Query<TranscationDTO>("SELECT * FROM transferdetail WHERE tranTime = @tranTime AND money = @money AND balance = @balance AND sAccountId = @sAccountId ORDER BY id DESC", first);
        }
    }
}
