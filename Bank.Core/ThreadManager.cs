﻿using leslie.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core
{
    /// <summary>
    /// Main Thread的管理
    /// </summary>
    public class ThreadManager : Singleton<ThreadManager>
    {
        private IRunOnMainThread container;
        /// <summary>
        /// 目前可以執行RunOnMainThread的物件
        /// </summary>
        public IRunOnMainThread CurrentContainer
        {
            set
            {
                container = value;
            }
        }

        /// <summary>
        /// 在Main Thread上執行action
        /// </summary>
        /// <param name="action"></param>
        public void RunOnMainThread(Action action)
        {
            container.RunOnMainThread(action);
        }
    }

    /// <summary>
    /// 在Main Thread上執行action
    /// </summary>
    public interface IRunOnMainThread
    {
        /// <summary>
        /// 在Main Thread上執行action
        /// </summary>
        /// <param name="action"></param>
        void RunOnMainThread(Action action);
    }
}
