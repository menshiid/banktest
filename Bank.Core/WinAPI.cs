﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core
{
    /// <summary>
    /// Win32 API 的宣告
    /// </summary>
    public class WinAPI
    {
        public const int GWL_ID = -12;
        public const int WM_GETTEXT = 0x000D;
        public const int WM_SETTEXT = 0x000C;
        public const int WM_CHAR = 0X102;
        private const int SW_HIDE = 0;
        private const int SW_SHOW = 5;

        [DllImport("User32")]
        private static extern int ShowWindow(int hwnd, int nCmdShow);

        [DllImport("User32.dll")]
        public static extern int GetWindowLong(IntPtr hWnd, int index);

        [DllImport("User32.dll", CharSet = CharSet.Unicode)]
        public static extern IntPtr SendDlgItemMessageW(IntPtr hWnd, int IDDlgItem, int uMsg, int nMaxCount, StringBuilder lpString);

        [DllImport("User32.dll")]
        public static extern IntPtr GetParent(IntPtr hWnd);


        [DllImport("user32.dll")]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool EnumChildWindows(IntPtr window,
                                                        EnumWindowProc callback,
                                                        IntPtr i);

        public delegate bool EnumWindowProc(IntPtr hWnd, IntPtr parameter);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowTextLength(IntPtr hWnd);

        [DllImport("user32.dll", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int GetWindowText(IntPtr hWnd,
                                                   StringBuilder lpString,
                                                   int nMaxCount);

        [DllImport("user32.dll", EntryPoint = "FindWindow", SetLastError = true)]
        public static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

        [DllImport("user32.dll", EntryPoint = "FindWindowEx", SetLastError = true)]
        public static extern IntPtr FindWindowEx(IntPtr hwndParent, uint hwndChildAfter, string lpszClass, string lpszWindow);
        [DllImport("user32.dll", EntryPoint = "SendMessage", SetLastError = true, CharSet = CharSet.Auto)]
        public static extern int SendMessage(IntPtr hwnd, uint wMsg, IntPtr wParam, IntPtr lParam);

        [DllImport("user32.dll", EntryPoint = "SetForegroundWindow", SetLastError = true)]
        public static extern void SetForegroundWindow(IntPtr hwnd);

        [DllImport("user32.dll")]
        public static extern int EnumChildWindows(IntPtr hWndParent, CallBack lpfn, int lParam);

        [DllImport("user32.dll")]
        public static extern uint GetWindowThreadProcessId(int hWnd, out uint lpdwProcessId);

        public delegate bool CallBack(IntPtr hwnd, int lParam);


        private string GetRichEditText(IntPtr hWndRichEdit)
        {
            Int32 dwID = GetWindowLong(hWndRichEdit, GWL_ID);
            IntPtr hWndParent = GetParent(hWndRichEdit);
            StringBuilder title = new StringBuilder(128);

            SendDlgItemMessageW(hWndParent, dwID,
                WM_GETTEXT,
                128, title);



            return (title.ToString());
        }

        private string SetRichEditText(IntPtr hWndRichEdit, string text)
        {
            Int32 dwID = GetWindowLong(hWndRichEdit, GWL_ID);
            IntPtr hWndParent = GetParent(hWndRichEdit);
            StringBuilder title = new StringBuilder(128);
            title.Append(text);

            SendDlgItemMessageW(hWndParent, dwID,
                WM_SETTEXT,
                128, title);



            return (title.ToString());
        }


        private IntPtr FindWindowEx(IntPtr hwnd, string lpszWindow, bool bChild)
        {
            IntPtr iResult = IntPtr.Zero;
            // 首先在父表單上查找控制項 
            iResult = FindWindowEx(hwnd, 0, null, lpszWindow);
            // 如果找到直接返回控制項句柄 
            if (iResult != IntPtr.Zero) return iResult;
            // 如果設定了不在子表單中查找 
            if (!bChild) return iResult;
            // 枚舉子表單，查找控制項句柄 
            int i = EnumChildWindows(hwnd, (h, l) =>
            {
                if (GetRichEditText(h) == lpszWindow)
                {
                    iResult = h;
                    return false;
                }
                return true;
            }, 0);
            // 返回查找結果 
            return iResult;
        }


        private static IntPtr FindPdfControlWindow(IntPtr parentHandle)
        {
            IntPtr result = IntPtr.Zero;
            IntPtr matchPointer = IntPtr.Zero;
            try
            {
                //allocate unmanaged memory for the result of the callback delegate
                matchPointer = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(IntPtr)));
                Marshal.WriteIntPtr(matchPointer, IntPtr.Zero);

                //instantiate the delegate and pass to the API
                EnumWindowProc windowChecker = CheckForPdfControlWindow;
                if (!EnumChildWindows(parentHandle,
                                                        windowChecker,
                                                        matchPointer))
                {
                }
            }
            finally
            {
                if (matchPointer != IntPtr.Zero) Marshal.FreeHGlobal(matchPointer);
            }
            return result;
        }

        private static bool CheckForPdfControlWindow(IntPtr handle,
                                              IntPtr matchPointer)
        {
            string title = GetWindowText(handle);
            Logger.Instance.Trace(title);
            if (title.Contains("请输入登录密码"))
            {
                Marshal.WriteIntPtr(matchPointer, handle);
                return false;
            }
            else
            {
                FindPdfControlWindow(handle);
            }
            return true;
        }

        private static string GetWindowText(IntPtr handle)
        {
            int captionLength = GetWindowTextLength(handle);
            StringBuilder buffer = new StringBuilder(captionLength + 1);
            GetWindowText(handle, buffer, buffer.Capacity);
            string title = buffer.ToString();
            return title;
        }

        public static void InputStr(IntPtr myIntPtr, string Input)
        {
            byte[] ch = (ASCIIEncoding.ASCII.GetBytes(Input));
            for (int i = 0; i < ch.Length; i++)
            {
                SendMessage(myIntPtr, WM_CHAR, (IntPtr)ch[i], IntPtr.Zero);
            }
        }

        public static void ShowWindow(int hWnd)
        {
            ShowWindow(hWnd, SW_SHOW);
        }

        public static void HideWindow(int hWnd)
        {
            ShowWindow(hWnd, SW_HIDE);
        }
    }
}
