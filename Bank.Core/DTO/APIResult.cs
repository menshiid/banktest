﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core.DTO
{
    public class APIResult
    {
        public int code { get; set; }
        public int max { get; set; }
        public string msg { get; set; }
    }
}
