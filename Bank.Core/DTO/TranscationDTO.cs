﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core.DTO
{
    /// <summary>
    /// 對應資料庫transferdetail表的結構物件
    /// </summary>
    public class TranscationDTO
    {
        public int id { get; set; }
        public int sAccountId { get; set; }
        public int tAccountId { get; set; }
        public int inOut { get; set; }
        public string money { get; set; }
        public string currency { get; set; }
        public string desc { get; set; }
        public decimal balance { get; set; }
        public DateTime tranTime { get; set; }
        public string eventId { get; set; }

        //2018.06.24 新增
        public string AccountName { get; set; }
        public string PayAccount { get; set; }
        public string OpeningAccountbank { get; set; }
        public string TransectionWay { get; set; }
        public string RecieverAccountName { get; set; }
        public string RecieverAccountEmailPhone { get; set; }
        public string RecieverOpeningAccountbank { get; set; }
        public string TransectionStatus { get; set; }
        public string Comment { get; set; }
        public string MoneyExchangeLogo { get; set; }

        //2018.06.29 新增
        public string CardNo { get; set; } //卡號
        public string AccountingDate { get; set; } //記帳日期
        public string TransectionFiled { get; set; } //交易場所
        public string TransectionFiledShortName { get; set; } //交易國家或地區簡稱
        public string TargetAccountName { get; set; } //對方戶名
        public string TransectionThrough { get; set; } //資金流向
        public string RestAmount { get; set; } //餘額
        public string TransectionAmount { get; set; } //交易金額
        public string BookinCurrency { get; set; } //記帳幣種
        public string BookingAmount { get; set; } //記帳金額

        //2019.03.20 新增
        public string Level { get; set; }

    }
}
