﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core.DTO
{
    /// <summary>
    /// 對應資料庫account表的結構物件
    /// </summary>
    public class AccountDTO
    {
        public int id { get; set; }
        public string account { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string name { get; set; }
        public bool isFetch { get; set; }
        public int bank { get; set; }
    }
}
