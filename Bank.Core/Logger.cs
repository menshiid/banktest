﻿using leslie.Utils;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using NLogger = NLog.Logger;

namespace Bank.Core
{
    /// <summary>
    /// 寫入Log檔
    /// </summary>
    public class Logger : Singleton<Logger>
    {
        private static NLogger logger = LogManager.GetCurrentClassLogger();
        public event Action<LogLevel, object> OnLog = null;

        public void Debug(object value)
        {
            logger.Debug(value);
            OnLog?.Invoke(LogLevel.Debug, value);
        }

        public void Error(object value)
        {
            System.Diagnostics.Debug.WriteLine(value.ToString());
            logger.Error(value);
            OnLog?.Invoke(LogLevel.Error, value);
        }

        public void Fatal(object value)
        {
            logger.Fatal(value);
            OnLog?.Invoke(LogLevel.Fatal, value);
        }

        public void Info(object value)
        {
            logger.Info(value);
            OnLog?.Invoke(LogLevel.Info, value);
        }

        public void Trace(object value)
        {
            logger.Trace(value);
            OnLog?.Invoke(LogLevel.Trace, value);
        }

        public void Warn(object value)
        {
            logger.Warn(value);
            OnLog?.Invoke(LogLevel.Warn, value);
        }

        public enum LogLevel
        {
            Debug,
            Info,
            Warn,
            Error,
            Trace,
            Fatal
        }
    }
}
