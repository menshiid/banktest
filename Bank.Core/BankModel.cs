﻿using Bank.Core.DTO;
using Banks;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bank.Core
{
    /// <summary>
    /// 銀行基礎類別
    /// </summary>
    public abstract class BankModel : IDisposable
    {
        /// <summary>
        /// 操作的IE物件
        /// </summary>
        protected IE IE;

        /// <summary>
        /// 銀行ID
        /// </summary>
        public abstract string BankID { get; }

        /// <summary>
        /// 銀行名稱
        /// </summary>
        public abstract string BankName { get; }

        /// <summary>
        /// 登入網址
        /// </summary>
        public abstract string LoginURL { get; }

        /// <summary>
        /// 銀行代碼
        /// </summary>
        public abstract int BankCode { get; }

        /// <summary>
        /// 登入的帳號
        /// </summary>
        protected AccountDTO SourceAccount { get; set; }

        /// <summary>
        /// 等級
        /// </summary>
        public string Level { get; set; }

        /// <summary>
        /// 是否有IE視窗開啟
        /// </summary>
        public bool HasIE
        {
            get
            {
                return IE != null;
            }
        }

        /// <summary>
        /// IE的Callback 文件下載完成
        /// </summary>
        /// <param name="url"></param>
        protected virtual void OnDocumentComplete(string url)
        {

        }

        /// <summary>
        /// 將帳密設定到IE畫面上
        /// </summary>
        /// <param name="Account"></param>
        public virtual void OnSetupAccount(AccountDTO Account)
        {
        }

        /// <summary>
        /// 錯誤發生時的處理
        /// </summary>
        /// <param name="e"></param>
        public virtual void OnError(Exception e)
        {

        }

        /// <summary>
        /// 取得交易
        /// </summary>
        public virtual void OnGetTranscations()
        {
            
        }

        /// <summary>
        /// 建立IE視窗
        /// </summary>
        /// <param name="Account"></param>
        public void CreateWindow(AccountDTO Account)
        {
            this.SourceAccount = Account;
            ViewManager.Instance.ShowLoadiing();
            IE = new IE();
            IE.Create(LoginURL, () =>
             {
                 IE.DocumentComplete += OnDocumentComplete;

                 ThreadManager.Instance.RunOnMainThread(() =>
                 {
                     OnSetupAccount(Account);
                     ViewManager.Instance.HideLoading();
                 });
             });
        }

        /// <summary>
        /// 關閉IE視窗
        /// </summary>
        public void CloseWindow()
        {
            IE?.Close();
        }

        /// <summary>
        /// 延遲millisecondsTimeout後在Main Thread執行action
        /// </summary>
        /// <param name="action">要執行的action</param>
        /// <param name="millisecondsTimeout">延遲時間(毫秒)</param>
        /// <remarks>會等待IE處於完成狀態才進行</remarks>
        protected void Run(Action action, int millisecondsTimeout)
        {
            Task.Run(() =>
            {
                Thread.Sleep(millisecondsTimeout);
                while (IE.ReadyState != SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
                {
                    Thread.Sleep(1000);
                }
                ThreadManager.Instance.RunOnMainThread(() =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                        OnError(e);
                    }
                });
            });
        }

        /// <summary>
        /// 延遲millisecondsTimeout後在Main Thread執行action
        /// </summary>
        /// <param name="action">要執行的action</param>
        /// <param name="millisecondsTimeout">延遲時間(毫秒)</param>
        /// <remarks>會等待IE處於完成狀態才進行</remarks>
        protected Task RunAwait(Action action, int millisecondsTimeout)
        {
            TaskCompletionSource<bool> tcs = new TaskCompletionSource<bool>();
            Task.Run(() =>
            {
                Thread.Sleep(millisecondsTimeout);
                while (IE.ReadyState != SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
                {
                    Thread.Sleep(1000);
                }
                ThreadManager.Instance.RunOnMainThread(() =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                        OnError(e);
                    }
                    tcs.SetResult(true);
                });
            });
            return tcs.Task;
        }

        /// <summary>
        /// 延遲millisecondsTimeout後在Main Thread執行action
        /// </summary>
        /// <param name="action">要執行的action</param>
        /// <param name="millisecondsTimeout">延遲時間(毫秒)</param>
        /// <remarks>會等待IE處於完成狀態才進行</remarks>
        protected Task<R> Run<R>(Func<R> action, int millisecondsTimeout)
        {
            return Task.Run<R>(() =>
            {
                TaskCompletionSource<R> task = new TaskCompletionSource<R>();
                Thread.Sleep(millisecondsTimeout);
                while (IE.ReadyState != SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
                {
                    Thread.Sleep(1000);
                }
                ThreadManager.Instance.RunOnMainThread(() =>
                {
                    try
                    {
                        task.SetResult(action());
                        return;
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                        OnError(e);
                    }
                    task.SetCanceled();
                });
                return task.Task;
            });
        }

        /// <summary>
        /// 立即在Main Thread執行action
        /// </summary>
        /// <param name="action"></param>
        /// <remarks>會等待IE處於完成狀態才進行</remarks>
        protected void Run(Action action)
        {
            Task.Run(() =>
            {
                while (IE.ReadyState != SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
                {
                    Thread.Sleep(1000);
                }
                ThreadManager.Instance.RunOnMainThread(() =>
                {
                    try
                    {
                        action();
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                        OnError(e);
                    }
                });
            });
        }

        /// <summary>
        /// 彈出訊息視窗
        /// </summary>
        /// <param name="message"></param>
        protected void Alert(string message)
        {
            ViewManager.Instance.Alert(message);
        }

        /// <summary>
        /// 釋放物件
        /// </summary>
        public virtual void Dispose()
        {
            IE?.Close();
        }


        public virtual void Stop()
        {

        }
    }
}
