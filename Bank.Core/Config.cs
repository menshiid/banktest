﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Bank.Core
{
    /// <summary>
    /// 設定檔
    /// </summary>
    public class Config
    {
        private const string FileName = "config.xml";

        private static Config config;
        public static Config Instance
        {
            get
            {
                if(config == null)
                {
                    if (File.Exists(FileName))
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Config));
                        using (var fs = new FileStream(FileName, FileMode.Open))
                        {
                            config = (Config)serializer.Deserialize(fs);
                        }
                    }
                    else
                    {
                        config = new Config();
                        XmlSerializer serializer = new XmlSerializer(typeof(Config));
                        using (var fs = new FileStream(FileName, FileMode.OpenOrCreate))
                        {
                            serializer.Serialize(fs, config);
                        }
                    }
                }
                return config;
            }
        }

        /// <summary>
        /// 資料庫連線
        /// </summary>
        public string Database { get; set; } = "server=192.168.1.73;uid=willy;pwd=557686@3;database=automoney";

        /// <summary>
        /// 重新整理的間格時間(毫秒)
        /// </summary>
        public int RefreshInterval { get; set; } = 2 * 60 * 100;

        /// <summary>
        /// 顯示Log
        /// </summary>
        public bool ShowLog { get; set; } = false;
    }
}
