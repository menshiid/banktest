﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using Dapper;
using MySql.Data.MySqlClient;
using Bank.Core.DTO;

namespace Bank.Core
{
    /// <summary>
    /// 資料庫存取層
    /// </summary>
    public class Database : IDisposable
    {
        /// <summary>
        /// 資料庫連線
        /// </summary>
        protected MySqlConnection connection;

        /// <summary>
        /// 建立資料庫連線物件
        /// </summary>
        /// <param name="AutoOpen">是否自動連線</param>
        public Database(bool AutoOpen = true)
        {
            if (AutoOpen)
            {
                Open();
            }
        }

        /// <summary>
        /// 釋放物件
        /// </summary>
        public void Dispose()
        {
            connection?.Dispose();
        }

        /// <summary>
        /// 開啟資料庫連線
        /// </summary>
        public void Open()
        {
            connection = new MySqlConnection(Config.Instance.Database);
            connection.Open();
        }

        /// <summary>
        /// 透過銀行代碼取得帳號
        /// </summary>
        /// <param name="BankCode">銀行代碼</param>
        /// <returns>該銀行的帳號</returns>
        public IEnumerable<AccountDTO> GetAccountsByCode(int BankCode)
        {
            return connection.Query<AccountDTO>("SELECT id, account, username, password, name, isFetch FROM account WHERE isLogin = 0 AND bank = " + BankCode);
        }

        /// <summary>
        /// 透過帳號取得帳號詳細資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>帳號的資料</returns>
        public IEnumerable<AccountDTO> GetAccountsByAccount(IEnumerable<string> account)
        {
            return connection.Query<AccountDTO>("SELECT id, account FROM account WHERE account IN ( '" + string.Join("','", account) + "' )");
        }


        /// <summary>
        /// 透過帳號取得帳號詳細資料
        /// </summary>
        /// <param name="account">帳號</param>
        /// <returns>帳號的資料</returns>
        public AccountDTO GetAccountByAccount(string account)
        {
            return connection.Query<AccountDTO>("SELECT * FROM account WHERE account = @account", new { account }).FirstOrDefault();
        }

        /// <summary>
        /// 建立新帳號
        /// </summary>
        /// <param name="account"></param>
        /// <returns></returns>
        public AccountDTO CreateAccount(AccountDTO account)
        {
            
            //var accountId = account.account;
            var id = connection.ExecuteScalar<int>("INSERT INTO account (username, password, account, name, bank, isLogin, isFetch) VALUES ('', '', " + account.account + ", '', " + account.bank + ", 0, '0');SELECT LAST_INSERT_ID();");
            account.id = id;
            return account;
        }

        /// <summary>
        /// 將帳號設定成登入中
        /// </summary>
        /// <param name="ID">帳號ID</param>
        /// <returns>是否設定成功</returns>
        public bool LoginAccount(int ID)
        {
            return connection.Execute("UPDATE account SET isLogin = 1 WHERE isLogin = 0 AND id = " + ID) == 1;
        }

        /// <summary>
        /// 將帳號設定成未登入
        /// </summary>
        /// <param name="ID">帳號ID</param>
        /// <returns>是否設定成功</returns>
        public bool LogoutAccount(int ID)
        {
            return connection.Execute("UPDATE account SET isLogin = 0 WHERE id = " + ID) == 1;
        }

        /// <summary>
        /// 將帳號設定成已擷取
        /// </summary>
        /// <param name="ID">帳號ID</param>
        /// <returns>是否設定成功</returns>
        public bool SetIsFetched(int ID)
        {
            return connection.Execute("UPDATE account SET isFetch = 1 WHERE id = " + ID) == 1;
        }

        /// <summary>
        /// 新增交易紀錄
        /// </summary>
        /// <param name="Trans">交易紀錄</param>
        public void AddTranscations(IEnumerable<TranscationDTO> Trans)
        {
            foreach(var t in Trans)
            {
                if (!string.IsNullOrEmpty(t.Comment))
                {
                    var pos = t.Comment.IndexOf("支付宝转账");
                    if (pos >= 0)
                    {
                        t.Comment = t.Comment.Substring(0, pos);
                        t.RecieverAccountEmailPhone = t.CardNo;
                    }
                }

                // 紀錄銀聯時的收款卡號
                if (!string.IsNullOrEmpty(t.desc))
                {
                    var pos = t.desc.IndexOf("银联入账");
                    {
                        t.RecieverAccountEmailPhone = t.CardNo;
                    }
                }
            }

            Trans = Trans.Reverse();
            foreach (var t in Trans) {
                t.id = connection.ExecuteScalar<int>("INSERT INTO transferdetail (sAccountId, tAccountId, `inOut`, money, currency, `desc`, balance, tranTime, eventId, AccountName, PayAccount, OpeningAccountbank, TransectionWay, RecieverAccountName, RecieverAccountEmailPhone, RecieverOpeningAccountbank, TransectionStatus, Comment, MoneyExchangeLogo, CardNo, AccountingDate, TransectionFiled, TransectionFiledShortName, TargetAccountName, TransectionThrough, RestAmount, TransectionAmount, BookinCurrency, BookingAmount, Level) VALUES (@sAccountId, @tAccountId, @inOut, @money, @currency, @desc, @balance,  @tranTime, @eventId, @AccountName, @PayAccount, @OpeningAccountbank, @TransectionWay, @RecieverAccountName, @RecieverAccountEmailPhone, @RecieverOpeningAccountbank, @TransectionStatus, @Comment, @MoneyExchangeLogo, @CardNo, @AccountingDate, @TransectionFiled, @TransectionFiledShortName, @TargetAccountName, @TransectionThrough, @RestAmount, @TransectionAmount, @BookinCurrency, @BookingAmount, @Level);SELECT LAST_INSERT_ID();", t);
            }
        }

        /// <summary>
        /// 更新交易紀錄狀態
        /// </summary>
        /// <param name="Trans">交易紀錄</param>
        public void UpdateIsSend(IEnumerable<TranscationDTO> Trans)
        {
            connection.Execute("UPDATE transferdetail SET is_send = 1 WHERE  id IN @id", new
            {
                id = Trans.Select(x => x.id)
            });
        }

        /// <summary>
        /// 取得最後的交易紀錄
        /// </summary>
        /// <param name="ID">來源帳號ID</param>
        /// <returns>交易紀錄</returns>
        public virtual IEnumerable<TranscationDTO> GetLastTranscations(int ID)
        {
            return connection.Query<TranscationDTO>("SELECT * FROM transferdetail WHERE sAccountId = @id AND  ORDER BY id DESC LIMIT 1", new { id = ID });
        }
    }
}
