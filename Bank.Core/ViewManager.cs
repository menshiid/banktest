﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace Banks
{
    /// <summary>
    /// View的管理物件
    /// </summary>
    public class ViewManager
    {
        public static ViewManager Instance { get; set; } = new ViewManager();

        /// <summary>
        /// 目前的Container
        /// </summary>
        public IContainer CurrentContainer { get; set; }

        /// <summary>
        /// 顯示一個View
        /// </summary>
        /// <param name="view"></param>
        public void ShowView(UserControl view)
        {
            CurrentContainer.ShowView(view);
        }

        /// <summary>
        /// 顯示Loading畫面
        /// </summary>
        public void ShowLoadiing()
        {
            CurrentContainer.ShowLoadiing();
        }

        /// <summary>
        /// 隱藏Loading畫面
        /// </summary>
        public void HideLoading()
        {
            CurrentContainer.HideLoading();
        }

        /// <summary>
        /// 顯示訊息視窗
        /// </summary>
        /// <param name="msg"></param>
        public void Alert(string msg)
        {
            CurrentContainer.Alert(msg);
        }

        public void UpdateCounter(string text)
        {
            CurrentContainer.UpdateCounter(text);
        }
    }

    /// <summary>
    /// Contaienr
    /// </summary>
    public interface IContainer
    {
        void ShowView(UserControl view);
        void ShowLoadiing();
        void HideLoading();
        void Alert(string msg);
        void UpdateCounter(string text);
    }
}
