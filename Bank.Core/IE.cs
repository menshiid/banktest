﻿using MSHTML;
using MSHTML;
using SHDocVw;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bank.Core
{
    /// <summary>
    /// IE的對應物件
    /// </summary>
    public class IE
    {
        /// <summary>
        /// IE的COM對應物件
        /// </summary>
        private IHTMLDocument2 ieDoc = null;
        /// <summary>
        /// IE視窗的COM對應物件
        /// </summary>
        private SHDocVw.InternetExplorer ieWnd = null;

        /// <summary>
        /// IE的狀態
        /// </summary>
        public tagREADYSTATE ReadyState
        {
            get
            {
                return ieWnd.ReadyState;
            }
        }

        /// <summary>
        /// 關閉所有IE視窗
        /// </summary>
        public static void CloseAll()
        {
            SHDocVw.ShellWindows shellWindows = new SHDocVw.ShellWindows();
            foreach (SHDocVw.InternetExplorer ie in shellWindows)
            {
                try
                {
                    if (ie.FullName.ToUpper().Contains("IEXPLORE.EXE"))
                    {
                        ie.Quit();
                    }
                }
                catch (Exception) { }
            }
        }

        private Dictionary<string, Action> onCompletes = new Dictionary<string, Action>();
        public delegate void DocumentCompleteEventHandler(string url);
        public DocumentCompleteEventHandler DocumentComplete;

        /// <summary>
        /// 建立IE視窗
        /// </summary>
        /// <param name="url">起始網址</param>
        /// <param name="onComplete">建立完成後的callback</param>
        public void Create(string url, Action onComplete)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo("IExplore.exe");
            startInfo.WindowStyle = ProcessWindowStyle.Maximized;
            startInfo.Arguments = url;

            var p = Process.Start(startInfo);

            bool found = false;
            while (!found)
            {
                Thread.Sleep(1000);

                SHDocVw.ShellWindows shellWindows = new SHDocVw.ShellWindows();
                foreach (SHDocVw.InternetExplorer ie in shellWindows)
                {
                    try
                    {
                        Debug.WriteLine("ie.HWND=" + ie.HWND);
                        Debug.WriteLine("p.MainWindowHandle=" + p.MainWindowHandle);
                        if (ie.HWND == (int)p.MainWindowHandle)
                        {
                            ieDoc = (IHTMLDocument2)ie.Document;
                            ieWnd = ie;
                            found = true;
                            break;
                        }
                    }
                    catch (Exception) { }
                }
            }
            if (ieWnd.ReadyState == SHDocVw.tagREADYSTATE.READYSTATE_COMPLETE)
            {
                onComplete?.Invoke();
            }
            else
            {
                onCompletes[url] = onComplete;
            }
            ieWnd.DocumentComplete += IeWnd_DocumentComplete;
        }

        /// <summary>
        /// IE文件顯示完成後的callabck
        /// </summary>
        /// <param name="pDisp"></param>
        /// <param name="URL"></param>
        private void IeWnd_DocumentComplete(object pDisp, ref object URL)
        {
            var url = $"{URL}";
            Logger.Instance.Trace("IeWnd_DocumentComplete: " + url);
            Action onComplete = null;
            if (onCompletes.TryGetValue(url, out onComplete))
            {
                onCompletes.Remove(url);
                onComplete?.Invoke();
            }
            DocumentComplete?.Invoke(url);
        }

        /// <summary>
        /// 執行一段JS
        /// </summary>
        /// <param name="js">javascript</param>
        public void RunJS(string js)
        {
            ieDoc.RunJS(js);
        }

        /// <summary>
        /// 在IE中利用名稱尋找input物件
        /// </summary>
        /// <param name="name">名稱</param>
        /// <returns></returns>
        public IEnumerable<HTMLInputElement> FindInputByName(string name)
        {
            return ieDoc.FindInputByName(name);
        }

        /// <summary>
        /// 在IE中利用id尋找input物件
        /// </summary>
        /// <param name="id">id</param>
        /// <returns></returns>
        public IHTMLElement FindInputByID(string id)
        {
            return ieDoc.FindElemetByID(id);
        }

        /// <summary>
        /// 在IE中利用css class尋找物件
        /// </summary>
        /// <param name="name">css name</param>
        /// <returns></returns>
        public IEnumerable<IHTMLElement> FindElemetByClassPart(string name)
        {
            return ieDoc.FindElemetByClassPart(name);
        }

        /// <summary>
        /// 取得frame
        /// </summary>
        /// <param name="index"></param>
        /// <param name="doc"></param>
        /// <returns></returns>
        public IHTMLDocument2 GetFrame(int index, IHTMLDocument2 doc = null)
        {
            if (doc == null)
            {
                doc = ieDoc;
            }
            Object frame_index = 0;

            if (doc.frames.length > index)
            {
                IHTMLWindow2 perbank_content_frame_window = (IHTMLWindow2)doc.frames.item(ref frame_index);
                return CrossFrameIE.GetDocumentFromWindow(perbank_content_frame_window);
            }
            else
            {
                return null;
            }
        }

        /// <summary>
        /// 讓IE顯示出來
        /// </summary>
        public void Show()
        {
            ieWnd.Visible = true;
            //WinAPI.ShowWindow(ieWnd.HWND);
        }

        /// <summary>
        /// 將IE隱藏起來
        /// </summary>
        public void Hide()
        {
            ieWnd.Visible = false;
            //WinAPI.HideWindow(ieWnd.HWND);
        }

        /// <summary>
        /// 關閉IE
        /// </summary>
        public void Close()
        {
            try
            {
                ieWnd.Quit();
            }
            catch (Exception) { }
        }
    }
}
