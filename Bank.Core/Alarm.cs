﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Bank.Core
{
    /// <summary>
    /// 鬧鐘
    /// </summary>
    public class Alarm
    {
        /// <summary>
        /// 週期(毫秒)
        /// </summary>
        public int Interval { get; set; }
        /// <summary>
        /// 鬧鐘觸發
        /// </summary>
        public event Action OnAlarm;

        protected Thread worker;
        protected AutoResetEvent sync = new AutoResetEvent(false);
        protected bool IsClose = false;

        /// <summary>
        /// 啟動鬧鐘(一次)
        /// </summary>
        public void Start()
        {
            Stop();
            worker = new Thread(() =>
            {
                IsClose = false;
                sync.WaitOne(Interval);
                if (!IsClose)
                {
                    OnAlarm?.Invoke();
                }
                worker = null;
            });
            worker.Start();
        }

        /// <summary>
        /// 啟動鬧鐘(重複)
        /// </summary>
        public void StartRepeat()
        {
            Stop();
            worker = new Thread(() =>
            {
                IsClose = false;
                while (!IsClose)
                {
                    sync.WaitOne(Interval);
                    if (!IsClose)
                    {
                        OnAlarm?.Invoke();
                    }
                }
                worker = null;
            });
            worker.Start();
        }

        /// <summary>
        /// 停止鬧鐘
        /// </summary>
        public void Stop()
        {
            if (worker != null)
            {
                IsClose = true;
                sync.Set();
                worker.Abort();
            }
        }
    }
}
