﻿using MSHTML;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank.Core
{
    /// <summary>
    /// IE的擴充物件
    /// </summary>
    public static class IEExtension
    {
        /// <summary>
        /// 利用TagName尋找物件
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IEnumerable<IHTMLElement> FindElemetByTagName(this IHTMLDocument2 ieDoc, string name)
        {
            return ieDoc.Where(ieElement => name.Equals(ieElement.tagName, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// 利用TagName尋找物件
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IEnumerable<IHTMLElement> FindElemetByTagName(this IHTMLElement ieDoc, string name)
        {
            return ieDoc.Where(ieElement => name.Equals(ieElement.tagName, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// 利用Name尋找Input物件
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static IEnumerable<HTMLInputElement> FindInputByName(this IHTMLDocument2 ieDoc, string name)
        {
            return ieDoc.FindElemetByTagName("input")
                        .Where(ieElement => name.Equals(((HTMLInputElement)ieElement).name, StringComparison.InvariantCultureIgnoreCase))
                        .Select(ieElement => ((HTMLInputElement)ieElement));
        }

        /// <summary>
        /// 利用ID尋找物件
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static IHTMLElement FindElemetByID(this IHTMLDocument2 ieDoc, string id)
        {
            return ieDoc.Where(ieElement => id.Equals(ieElement.id, StringComparison.InvariantCultureIgnoreCase)).FirstOrDefault();
        }

        /// <summary>
        /// 利用css class尋找物件
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <remarks>css class 要完全相等</remarks>
        public static IEnumerable<IHTMLElement> FindElemetByClass(this IHTMLDocument2 ieDoc, string cls)
        {
            return ieDoc.Where(ieElement => cls.Equals(ieElement.className, StringComparison.InvariantCultureIgnoreCase));
        }

        /// <summary>
        /// 利用css class尋找物件
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="cls"></param>
        /// <returns></returns>
        /// <remarks>比對部分css class</remarks>
        public static IEnumerable<IHTMLElement> FindElemetByClassPart(this IHTMLDocument2 ieDoc, string cls)
        {
            return ieDoc.Where(ieElement => ieElement.className != null && ieElement.className.Contains(cls));
        }

        /// <summary>
        /// 執行一段JS
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="js"></param>
        public static void RunJS(this IHTMLDocument2 ieDoc, string js)
        {
            ThreadManager.Instance.RunOnMainThread(() =>
            {
                ieDoc.parentWindow.execScript(js);
            });
        }

        /// <summary>
        /// 就是where
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        private static List<IHTMLElement> Where(this IHTMLDocument2 ieDoc, Func<IHTMLElement, bool> where)
        {
            List<IHTMLElement> list = new List<IHTMLElement>();
            foreach (Object item in ieDoc.all)
            {
                if(item is IHTMLElement)
                {
                    IHTMLElement ieElement = item as IHTMLElement;
                    if (where.Invoke(ieElement))
                    {
                        list.Add(ieElement);
                    }
                }
            }
            return list;
        }

        /// <summary>
        /// 就是where
        /// </summary>
        /// <param name="ieDoc"></param>
        /// <param name="where"></param>
        /// <returns></returns>
        private static List<IHTMLElement> Where(this IHTMLElement ieDoc, Func<IHTMLElement, bool> where)
        {
            List<IHTMLElement> list = new List<IHTMLElement>();
            foreach (IHTMLElement ieElement in ieDoc.all)
            {
                if (where.Invoke(ieElement))
                {
                    list.Add(ieElement);
                }
            }
            return list;
        }
    }
}
