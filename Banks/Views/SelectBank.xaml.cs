﻿using Banks.Controls;
using Banks.Manager;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Banks.Views
{
    /// <summary>
    /// Interaction logic for SelectBank.xaml
    /// </summary>
    public partial class SelectBank : UserControl
    {

        public SelectBank()
        {
            InitializeComponent();
        }

        private void UserControl_Loaded(object sender, RoutedEventArgs e)
        {
            foreach(var bank in BankManager.Instance.Banks)
            {
                var v = new BankButon(bank);
                v.Click += BankButton_Click;
                pnlContainer.Children.Add(v);
            }
        }

        private void BankButton_Click(object sender, EventArgs e)
        {
            BankButon button = sender as BankButon;
            BankLogin loginView = new BankLogin(button.Bank);
            ViewManager.Instance.ShowView(loginView);
        }
    }
}
