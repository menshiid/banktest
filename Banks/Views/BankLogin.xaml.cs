﻿using Bank.Core;
using Banks.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Banks.Views
{
    /// <summary>
    /// Interaction logic for BankLogin.xaml
    /// </summary>
    public partial class BankLogin : UserControl
    {
        private LoginVM LoginVM { get; set; } = new LoginVM();
        private BankModel bank;

        public BankLogin(BankModel bank)
        {
            InitializeComponent();

            this.bank = bank;
            this.DataContext = LoginVM;
            LoginVM.BankName = bank.BankName;
            LoadAccount();
        }

        private void LoadAccount()
        {
            var accounts = new List<AccountVM>();
            //using (Database db = new Database())
            //{
            //    accounts.AddRange(db.GetAccountsByCode(bank.BankCode)
            //                        .Select(x => new AccountVM(x)));
            //}

            cbAccount.ItemsSource = accounts;
        }

        private void cbAccount_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            btnLogin_Click(sender, e);
        }

        private void OpenIE()
        {
            bank.CreateWindow(null);
            //AccountVM account = cbAccount.SelectedItem as AccountVM;
            //if (account != null)
            //{
            //    bool isLogin = true;
            //    //bool isLogin = false;

            //    //暫時註解不然每次都要進去DB改...XD
            //    //using (Database db = new Database())
            //    //{
            //    //    isLogin = db.LoginAccount(account.ID);
            //    //}

            //    if (isLogin)
            //    {
            //        bank.CreateWindow(account.Source);
            //    }
            //    else
            //    {
            //        ViewManager.Instance.Alert("帐号已被登入，请重新选择");
            //        LoadAccount();
            //    }
            //}
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
            if (bank.HasIE)
            {
                bank.CloseWindow();
                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    Dispatcher.Invoke(OpenIE);
                });
            }
            else
            {
                OpenIE();
            }
        }

        private void btnGetTranscations_Click(object sender, RoutedEventArgs e)
        {
            bank.OnGetTranscations();
        }

        private void btnBack_Click(object sender, RoutedEventArgs e)
        {
            bank.Dispose();
            ViewManager.Instance.ShowView(new SelectBank());
        }

        private void btnOpenIE_Click(object sender, RoutedEventArgs e)
        {
            bank.Level = (cbLevel.SelectedItem as ComboBoxItem).Content.ToString();
            if (bank.HasIE)
            {
                bank.CloseWindow();
                Task.Run(() =>
                {
                    Thread.Sleep(5000);
                    Dispatcher.Invoke(OpenIE);
                });
            }
            else
            {
                OpenIE();
            }

        }
    }
}
