﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Banks
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            var args = Environment.GetCommandLineArgs();
            if (!args.Contains("debug"))
            {
                AppDomain.CurrentDomain.UnhandledException += CurrentDomain_UnhandledException; ;
                Application.Current.DispatcherUnhandledException += Current_DispatcherUnhandledException;         // Uncomment the following after testing to see that NBug is working as configured         
                // NBug.Settings.ReleaseMode = true;   
            }
        }

        private void Current_DispatcherUnhandledException(object sender, System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {
            try
            {
                NBug.Handler.DispatcherUnhandledException.Invoke(sender, e);
            }
            catch (Exception) { }
            Environment.Exit(0);
        }

        private void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            try
            {
                NBug.Handler.UnhandledException.Invoke(sender, e);
            }
            catch (Exception) { }
            Environment.Exit(0);
        }
    }
}
