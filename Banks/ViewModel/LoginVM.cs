﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banks.ViewModel
{
    public class LoginVM : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private byte[] authCodeImage;
        public byte[] AuthCodeImage
        {
            get
            {
                return authCodeImage;
            }
            set
            {
                authCodeImage = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(AuthCodeImage)));
            }
        }

        private string bankName;
        public string BankName
        {
            get
            {
                return bankName;
            }
            set
            {
                bankName = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(BankName)));
            }
        }
    }
}
