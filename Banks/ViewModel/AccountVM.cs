﻿using Bank.Core.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Banks.ViewModel
{
    public class AccountVM
    {
        public AccountVM()
        {
        }

        public AccountVM(AccountDTO dto)
        {
            Source = dto;
        }

        public int ID
        {
            get
            {
                return Source.id;
            }
        }

        public string Name
        {
            get
            {
                return Source.name;
            }
        }
        public string Account
        {
            get
            {
                return Source.account;
            }
        }
        public string Password
        {
            get
            {
                return Source.password;
            }
        }

        public AccountDTO Source { get; set; }
    }
}
