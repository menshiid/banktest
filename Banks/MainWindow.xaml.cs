﻿using Bank.Core;
using Banks.Dialog;
using Banks.ViewModel;
using Banks.Views;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Banks
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, IContainer, IRunOnMainThread
    {
        public MainWindow()
        {
            InitializeComponent();

            ViewManager.Instance.CurrentContainer = this;
            ThreadManager.Instance.CurrentContainer = this;
        }


        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.Topmost = true;

            ViewManager.Instance.ShowView(new SelectBank());

            IE.CloseAll();

            JsonConvert.SerializeObject(new { });

            if (Config.Instance.ShowLog)
            {
                Logger.Instance.OnLog += Logger_OnLog;
            }
            else
            {
                txtLog.Visibility = Visibility.Collapsed;
            }

        }

        private void Logger_OnLog(Logger.LogLevel level, object value)
        {
            WriteLog(value.ToString());
        }

        public void ShowView(UserControl view)
        {
            container.Children.Clear();
            container.Children.Add(view);
        }

        public static string doWeirdMapping(string arg)
        {
            Encoding w1252 = Encoding.GetEncoding(1252);
            return Encoding.UTF8.GetString(w1252.GetBytes(arg));
        }

        public void RunOnMainThread(Action action)
        {
            try
            {
                Dispatcher.Invoke(() =>
                {
                    try
                    {
                        action?.Invoke();
                    }
                    catch (Exception e1)
                    {
                        Logger.Instance.Error(e1);
                    }
                });
            }
            catch (Exception e)
            {
                Logger.Instance.Error(e);
            }
        }

        public void ShowLoadiing()
        {
            RunOnMainThread(() => gLoading.Visibility = Visibility.Visible);
        }

        public void HideLoading()
        {
            RunOnMainThread(() => gLoading.Visibility = Visibility.Collapsed);
        }

        public void Alert(string msg)
        {
            //RunOnMainThread(() => new AlertWindow(msg).ShowDialog());
            System.Windows.MessageBox.Show(msg);


        }

        public void UpdateCounter(string text)
        {
            RunOnMainThread(() =>
            {
                counter.Content = text;
            });
        }

        private int txtLogLineCount = 0;
        public void WriteLog(string msg)
        {
            WriteLog(msg, Colors.Black);
        }
        private void WriteLog(string msg, System.Windows.Media.Color color )
        {
            Dispatcher.Invoke(() =>
            {
                try
                {
                    if (txtLogLineCount > 500)
                    {
                        TextRange txt = new TextRange(txtLog.Document.ContentStart, txtLog.Document.ContentEnd);
                        txt.Text = "";
                        txtLogLineCount = 0;
                    }
                    msg = string.Format("[{0:HH:mm:ss}] {1}\n", DateTime.Now, msg);
                    TextRange rangeOfWord = new TextRange(txtLog.Document.ContentEnd, txtLog.Document.ContentEnd);
                    rangeOfWord.Text = msg;
                    rangeOfWord.ApplyPropertyValue(TextElement.ForegroundProperty, new SolidColorBrush(color));
                    txtLog.ScrollToEnd();

                    Debug.WriteLine(msg);
                }
                catch (Exception) { }
            });
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Logger.Instance.OnLog -= Logger_OnLog;
        }
    }
}
