﻿using Bank.Core;
using leslie.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Banks.Manager
{
    public class BankManager : Singleton<BankManager>
    {
        private Dictionary<string, BankModel> banks = new Dictionary<string, BankModel>();

        public IEnumerable<BankModel> Banks
        {
            get
            {
                Load();
                return banks.Values;
            }
        }

        public void Load()
        {
            if (banks.Count == 0)
            {
                foreach (var dll in Directory.EnumerateFiles(@"Bank\", "*.dll"))
                {
                    Assembly assembly = Assembly.LoadFile(Path.GetFullPath(dll));
                    foreach (var m in assembly.Modules)
                    {
                        foreach (var t in m.GetTypes())
                        {
                            if (t.IsClass && t.IsSubclassOf(typeof(BankModel)))
                            {
                                BankModel b = (BankModel)Activator.CreateInstance(t);
                                banks.Add(b.BankID, b);
                            }
                        }
                    }
                }
            }
        }
    }
}
