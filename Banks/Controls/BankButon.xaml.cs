﻿using Bank.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Banks.Controls
{
    /// <summary>
    /// Interaction logic for BankButon.xaml
    /// </summary>
    public partial class BankButon : UserControl
    {
        private BankModel bank;
        public event EventHandler Click;

        public BankModel Bank
        {
            get
            {
                return bank;
            }
        }

        public BankButon(BankModel bank)
        {
            InitializeComponent();

            this.bank = bank;
            this.DataContext = bank;
        }

        private void button_click(object sender, RoutedEventArgs e)
        {
            Click?.Invoke(this, e);
        }
    }
}
