﻿
function find() {
    var f1 = $('#perbank-content-frame');
    if (f1 && f1.length > 0) {
        var f2 = $('#content-frame', $('#perbank-content-frame').contents());
        if (f2 && f2.length > 0) {
            var f3 = $('.handle-ul', $('#content-frame', $('#perbank-content-frame').contents()).contents());
            if (f3 && f3.length > 0) {
                $('.handle-ul', $('#content-frame', $('#perbank-content-frame').contents()).contents()).children()[0].click();
                return;
            }
        }
    }
    setTimeout(find, 1000);
}

find();