﻿using Bank.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Bank.Core.DTO;

namespace icbc
{
    /// <summary>
    /// 資料庫存取層
    /// </summary>
    public class ICBCDatabase : Database
    {
        /// <summary>
        /// 取得最終幾筆的交易紀錄
        /// </summary>
        /// <param name="ID"></param>
        /// <returns></returns>
        public override IEnumerable<TranscationDTO> GetLastTranscations(int ID)
        {
            //return connection.Query<TranscationDTO>("SELECT * FROM transferdetail WHERE tranTime >= ( SELECT tranTime FROM transferdetail WHERE sAccountId = @id AND eventId LIKE 'h%' ORDER BY tranTime DESC LIMIT 1 ) AND sAccountId = @id ORDER BY tranTime DESC", new { id = ID });
            return connection.Query<TranscationDTO>("SELECT * FROM transferdetail WHERE tranTime >= ( SELECT tranTime FROM transferdetail WHERE sAccountId = @id AND eventId LIKE 'h%' LIMIT 1 ) AND sAccountId = @id", new { id = ID });
        }
    }
}
