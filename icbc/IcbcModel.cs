﻿using Bank.Core;
using Bank.Core.DTO;
using Banks;
using icbc.Properties;
using MSHTML;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace icbc
{
    /// <summary>
    /// 中国工商银行
    /// </summary>
    public class IcbcModel : BankModel
    {
        public override string BankName => "中国工商银行";

        public override string LoginURL => "https://mybank.icbc.com.cn/icbc/newperbank/perbank3/frame/frame_index.jsp";

        public override string BankID => "ICBC";

        public override int BankCode => 102;

        private int count = 1;

        /// <summary>
        /// 目前模式
        /// </summary>
        private Modes CurrentMode = Modes.GetHistory;

        /// <summary>
        /// 目前的交易紀錄
        /// </summary>
        private List<TranscationData> CurrentTranscationData = new List<TranscationData>();
        /// <summary>
        /// 目前處理到哪一筆
        /// </summary>
        private int CurrentRowIndex = 0;

        /// <summary>
        /// 狀態
        /// </summary>
        private enum Status
        {
            /// <summary>
            /// 停止中
            /// </summary>
            Idle,
            /// <summary>
            /// 開始取得交易紀錄
            /// </summary>
            StartGetTranscation,
            /// <summary>
            /// 帳號清單
            /// </summary>
            AccountList,
            /// <summary>
            /// 已取得帳號清單
            /// </summary>
            OnAccountListComplete,
            /// <summary>
            /// 開始查詢
            /// </summary>
            StartQuery,
            /// <summary>
            /// 取得交易紀錄
            /// </summary>
            GetTranscation,
            /// <summary>
            /// 取得交易詳細記錄
            /// </summary>
            GetTranscationDetail,
            /// <summary>
            /// 取得批次處理詳細記錄
            /// </summary>
            GetBatchDetail,
            /// <summary>
            /// 登入
            /// </summary>
            Login
        }

        /// <summary>
        /// 模式
        /// </summary>
        private enum Modes
        {
            /// <summary>
            /// 擷取歷史資料
            /// </summary>
            GetHistory,
            /// <summary>
            /// 監控新資料
            /// </summary>
            WatchNewData,
            /// <summary>
            /// 等待中
            /// </summary>
            Waiting
        }

        /// <summary>
        /// 目前狀態
        /// </summary>
        private Status CurrentStatus = Status.Idle;

        /// <summary>
        /// 將帳密設定到IE畫面上
        /// </summary>
        /// <param name="Account"></param>
        public override void OnSetupAccount(AccountDTO Account)
        {
            base.OnSetupAccount(Account);

            CurrentStatus = Status.Login;

            //Debug.WriteLine($"account={Account.username} password={Account.password}");
            //var js = Resources.OnSetupAccount.Replace("{account}", Account.username).Replace("{password}", Account.password);
            //Run(() => IE.RunJS(js));
        }

        /// <summary>
        /// 取得交易
        /// </summary>
        public override void OnGetTranscations()
        {
            this.CurrentMode = !SourceAccount.isFetch ? Modes.GetHistory : Modes.WatchNewData;
            CurrentStatus = Status.StartGetTranscation;
            Run(() => IE.RunJS("$('.link-div',$('#content-frame',$('#perbank-content-frame').contents()).contents()).children()[0].click()"));
            ViewManager.Instance.ShowLoadiing();
        }

        /// <summary>
        /// 將交易記錄存入資料庫
        /// </summary>
        /// <param name="data">交易記錄</param>
        private void SaveToDatabase(IEnumerable<TranscationData> data)
        {
            Logger.Instance.Info($"Save to database: " + data.Count() + " rows.");
            try
            {
                //File.WriteAllText("test.json", JsonConvert.SerializeObject(data));
                Dictionary<string, int> map = null;
                using (Database db = new Database())
                {
                    map = db.GetAccountsByAccount(data.Select(x => x.TargetAccount))
                                 .ToDictionary(x => x.account, x => x.id);

                    var src = data.Where(x => !x.IsSaved).Select(x =>
                     {
                         int id = -1;
                         if (!string.IsNullOrEmpty(x.TargetAccount))
                         {
                             map.TryGetValue(x.TargetAccount, out id);
                         }
                         var dto = new TranscationDTO()
                         {
                             sAccountId = SourceAccount.id,
                             tAccountId = id,
                             money = x.Money.ToString(),
                             balance = x.Balance,
                             desc = x.Desc,
                             currency = "RMB",
                             inOut = x.Money > 0 ? 1 : 2,
                             tranTime = x.Date,
                             eventId = x.Type == TranscationType.Normal ? "h" + x.EventID : "d",
                             AccountName = x.AccountName,
                             PayAccount = x.PayAccount,
                             OpeningAccountbank = x.OpeningAccountbank,
                             TransectionWay = x.TransectionWay,
                             RecieverAccountName = x.RecieverAccountName,
                             RecieverAccountEmailPhone = x.RecieverAccountEmailPhone,
                             RecieverOpeningAccountbank = x.RecieverOpeningAccountbank,
                             TransectionStatus = x.TransectionStatus,
                             Comment = x.Comment,
                             MoneyExchangeLogo = x.MoneyExchangeLogo,
                             CardNo = x.CardNo,
                             AccountingDate = x.AccountingDate,
                             TransectionFiled = x.TransectionFiled,
                             TransectionFiledShortName = x.TransectionFiledShortName,
                             TargetAccountName = x.TargetAccountName,
                             TransectionThrough = x.TransectionThrough,
                             RestAmount = x.RestAmount,
                             TransectionAmount = x.TransectionAmount,
                             BookinCurrency = x.BookinCurrency,
                             BookingAmount = x.BookingAmount,
                             Level = x.Level
                         };
                         return dto;
                     }).ToList();
                    //TransectionStatus.IndexOf("支付宝");
                    
                    db.AddTranscations(src);

                    foreach (var x in src)
                    {
                        try
                        {
                            var name = x.AccountName;
                            if (string.IsNullOrEmpty(x.AccountName))
                            {
                                name = x.Comment;
                            }

                            var pdata = new
                            {
                                trans_date = x.tranTime.ToString("yyyy-MM-dd HH:mm:ss"),
                                inmoney = x.money,
                                balance = x.balance,
                                trans_type = "",
                                trans_pay_name = name,
                                trans_bz = "",
                                level = x.Level,
                                tid = x.id,
                                card_no = ""
                            };

                            var sign = string.Join("", MD5.Create().ComputeHash(System.Text.Encoding.UTF8.GetBytes($"{pdata.tid}16*akl8#1f")).Select(y => y.ToString("x2")));

                            var resultString = DefaultDownloader.Instance.DownloadString("http://test.eb007.xyz/icbcrev/rev", PostData:  $"data=[{JsonConvert.SerializeObject(pdata)}]&sign={sign}");
                            Logger.Instance.Trace("Call API: " + x.id + " result: " + resultString);

                            var result = JsonConvert.DeserializeObject<APIResult>(resultString);
                            if (result.code == 1)
                            {
                                db.UpdateIsSend(new TranscationDTO[] { x });
                            }

                        }
                        catch (Exception e)
                        {
                            Logger.Instance.Error(e);
                        }
                    }
                }

                foreach (var d in data)
                {
                    d.IsSaved = true;
                }
            }
            catch (Exception e)
            {
                Logger.Instance.Error(e);
                Debug.WriteLine(e);
            }
        }

        /// <summary>
        /// 定時鬧鐘
        /// </summary>
        private Alarm alarm;

        /// <summary>
        /// 進入等待模式
        /// </summary>
        private void GotoWaiting()
        {
            ViewManager.Instance.HideLoading();
            CurrentMode = Modes.Waiting;
            alarm?.Stop();
            alarm = new Alarm();
            alarm.Interval = Config.Instance.RefreshInterval;
            alarm.OnAlarm += Alarm_OnAlarm;
            alarm.Start();

            Logger.Instance.Info("Waiting for next scan.");
        }

        /// <summary>
        /// 鬧鐘時間到
        /// </summary>
        private void Alarm_OnAlarm()
        {
            ViewManager.Instance.ShowLoadiing();
            CurrentMode = Modes.WatchNewData;
            CurrentStatus = Status.GetTranscation;
            Run(() =>
            {
                //按下查詢按鈕
                IE.RunJS("$('#chaxun',$('#content-frame',$('#perbank-content-frame').contents()).contents()).click();");
                ViewManager.Instance.UpdateCounter($"{count++}");
            });
            Logger.Instance.Info("New scan start....");
        }

        private Dictionary<string, Expression<Func<TranscationData, string>>> TranscationDataMapper = new Dictionary<string, Expression<Func<TranscationData, string>>>()
        {
            //一般交易
            {"付款人", t=> t.AccountName },{"付款账号", t=> t.OpeningAccountbank },
            {"付款账号开户行", t=> t.OpeningAccountbank },{"交易渠道", t=> t.TransectionWay },
            {"收款人", t=> t.RecieverAccountName },{"收款账号/手机号", t=> t.RecieverAccountEmailPhone },
            {"收款账号开户行：", t=>t.RecieverOpeningAccountbank },{"交易状态", t=> t.TransectionStatus },
            {"附言", t=> t.Comment },{"钞汇标志", t=> t.MoneyExchangeLogo },
            //批次處理
            {"卡号", t=> t.CardNo },{"记账日期", t=> t.AccountingDate },
            {"交易场所", t=> t.TransectionFiled },{"交易国家或地区简称", t=> t.TransectionFiledShortName },
            {"对方户名", t=> t.TargetAccountName },{"资金流向", t=> t.TransectionThrough },
            {"余额", t=> t.RestAmount},{"交易金额", t=> t.TransectionAmount },
            {"记账币种", t=> t.BookinCurrency },{"记账金额", t=> t.BookingAmount },
            {"摘要", t=> t.Comment }
        };

        private void SetMethod<T, R>(Expression<Func<T, R>> expression, T target, R value)
        {
            var memberExpression = (MemberExpression)expression.Body;
            var property = (PropertyInfo)memberExpression.Member;
            var setMethod = property.GetSetMethod();
            setMethod?.Invoke(target, new object[] { value });
        }

        /// <summary>
        /// IE的Callback 文件下載完成
        /// </summary>
        /// <param name="url"></param>
        protected override void OnDocumentComplete(string url)
        {
            if (url.Contains("perbank3/includes/mybank1_new.jsp") && CurrentStatus == Status.Login)
            {
                Logger.Instance.Info("Parsing account list....");
                Run(() =>
                {
                    IE.RunJS("$('script',$('.link-div',$('#content-frame',$('#perbank-content-frame').contents()).contents())).remove();$('.link-div').remove();$(document.body).append($('.link-div',$('#content-frame',$('#perbank-content-frame').contents()).contents()).parent().html());$('a:first',$('.link-div')).attr('id', 'mainlink');");
                    try
                    {
                        var mainlink = IE.FindInputByID("mainlink");
                        var href = mainlink.getAttribute("href")?.ToString();
                        //javascript:AtomSerivceSubmit('PBL200711','6212260403020161697||no|000| |myacct');
                        Match m = Regex.Match(href, @"['](?<id>\d+)[|]");
                        if (m.Success)
                        {
                            var account = m.Groups["id"].Value;
                            using (Database db = new Database())
                            {
                                var acc = db.GetAccountByAccount(account);
                                if (acc == null)
                                {
                                    acc = new AccountDTO()
                                    {
                                        account = account,
                                        isFetch = false,
                                        bank = BankCode
                                    };
                                    acc = db.CreateAccount(acc);
                                }
                                this.SourceAccount = acc;
                            }
                            OnGetTranscations();
                            CurrentStatus = Status.OnAccountListComplete;
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                    }
                });
            }
            /*else if (url.Contains("perbank3/includes/mybank.jsp") && CurrentStatus == Status.StartGetTranscation)
            {
                Run(() =>
                {
                    IE.RunJS("$('.handle-ul', $('#content-frame', $('#perbank-content-frame').contents()).contents()).children()[0].click();");
                    CurrentStatus = Status.AccountList;
                });
            }
            else if (url.Contains("operationName=per_AccountQueryHisdetailOp") && CurrentStatus == Status.AccountList)
            {
                CurrentStatus = Status.OnAccountListComplete;
            }*/
            else if (url.Contains("ICBCINBSReqServlet") && CurrentStatus == Status.OnAccountListComplete)
            {
                Run(() =>
                {
                    if (CurrentStatus == Status.OnAccountListComplete)
                    {
                        IE.RunJS(@"$(document).ready(function(){
var frame = $('#content-frame',$('#perbank-content-frame').contents()).contents();
$('.ebdp-pc4promote-datepicker-button[num=-1][type=0]',frame).click();
$('#DRCRFRadio .ebdp-pc4promote-radio-radius-tab:nth-child(2)', frame).click()
$('#chaxun', frame).click();}
)");
                        Logger.Instance.Info("run js: " + CurrentStatus);
                        CurrentStatus = Status.GetTranscation;
                    }
                }, 10000);
            }
            else if (url.Contains("ICBCINBSReqServlet") && CurrentStatus == Status.GetTranscation)
            {
                Logger.Instance.Info("Parsing transcation list....");
                Action GetNext = () =>
                {
                    try
                    {
                        while (true)
                        {
                            CurrentRowIndex++;

                            //如果已經沒有資料的話跳出
                            if (CurrentRowIndex >= CurrentTranscationData.Count)
                            {
                                break;
                            }

                            Logger.Instance.Info($"Current Row : {CurrentRowIndex}");
                            //Logger.Instance.Info($"Current Row : {CurrentRowIndex}/{CurrentTranscationData.Count}");

                            var data = CurrentTranscationData[CurrentRowIndex];
                            //如果這一筆資料已經存過表示後面的都存了
                            if (data.IsSaved)
                            {
                                CurrentRowIndex = CurrentTranscationData.Count;
                                break;
                            }
                            //如果資料類型是一般交易的話去取得詳細資料
                            if (data.Type == TranscationType.Normal)
                            {
                                CurrentStatus = Status.GetTranscationDetail;
                                IE.RunJS(data.Goto);
                                break;
                            }
                            else
                            {
                                CurrentStatus = Status.GetBatchDetail;
                                IE.RunJS(data.Goto);
                                break;
                            }
                        }
                        //如果已經完成了的話就存入DB
                        if (CurrentRowIndex >= CurrentTranscationData.Count)
                        {
                            //存入DB
                            SaveToDatabase(CurrentTranscationData);

                            //設定成已擷取
                            if (CurrentMode == Modes.GetHistory && !SourceAccount.isFetch)
                            {
                                using (Database db = new Database())
                                {
                                    db.SetIsFetched(SourceAccount.id);
                                }
                                SourceAccount.isFetch = true;
                            }

                            //進入等待模式
                            GotoWaiting();

                            ViewManager.Instance.HideLoading();

                            //Alert("[DEBUG]操作完成");
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                    }
                };

                //開始取得交易紀錄
                Action<IEnumerable<TranscationData>> StartGetTranscation = data =>
                {
                    //如果資料中有未儲存項目
                    if (data.Any(x => !x.IsSaved))
                    {
                        CurrentMode = Modes.GetHistory;
                        CurrentTranscationData = data.ToList();
                        CurrentRowIndex = -1;

                        GetNext();
                    }
                    else
                    {
                        //沒有的話進入等待狀態
                        GotoWaiting();
                    }
                };

                Run(() =>
                {
                    if (CurrentMode == Modes.GetHistory)
                    {
                        if (CurrentTranscationData.Count > 0)
                        {
                            //每一筆交易紀錄都會走這條路
                            GetNext();
                        }
                        else
                        {
                            //取得畫面中的交易紀錄
                            GetCurrentTranscationTable(data =>
                            {
                                //開始擷取資料
                                StartGetTranscation(data);
                            });
                        }
                    }
                    else if (CurrentMode == Modes.WatchNewData)
                    {
                        //比較交易紀錄
                        Action<List<TranscationData>, List<TranscationData>> CompareTranscationData = (data, trans) =>
                        {
                            if (data.Count == 0)
                            {
                                //沒有新的資料，進入等待模式
                                GotoWaiting();
                                return;
                            }

                            //最後一筆一般交易的index
                            var lastHistoryIndexInTrans = -1;
                            for (int i = 0; i < trans.Count; i++)
                            {
                                if (trans[i].Type == TranscationType.Normal)
                                {
                                    lastHistoryIndexInTrans = i;
                                    break;
                                }
                            }
                            if (trans.Count < 0)
                            {
                                //資料庫內沒有資料
                                StartGetTranscation(data);
                            }
                            else
                            {
                                if (lastHistoryIndexInTrans < 0)
                                {
                                    //沒有歷史紀錄
                                    int count = data.Count - trans.Count;
                                    if (count <= 0)
                                    {
                                        //進入等待模式
                                        GotoWaiting();
                                        return;
                                    }
                                    //跳過已經擷取的資料並且開始擷取新資料
                                    StartGetTranscation(data.Take(count).Concat(trans));
                                }
                                else
                                {
                                    //最後一筆的EventID
                                    var lastEventID = trans[lastHistoryIndexInTrans].EventID;
                                    //最後一筆的index
                                    int pos = -1;
                                    for (int i = 0; i < data.Count; i++)
                                    {
                                        if (data[i].EventID == lastEventID)
                                        {
                                            pos = i;
                                            break;
                                        }
                                    }
                                    if (pos < 0)
                                    {
                                        //全部資料都是新的
                                        StartGetTranscation(data.Concat(trans));
                                    }
                                    else if (pos == 0)
                                    {
                                        //沒有新的資料，進入等待模式
                                        GotoWaiting();
                                        return;
                                    }
                                    else
                                    {
                                        //跳過已經擷取的資料並且開始擷取新資料
                                        StartGetTranscation(data.Take(pos - lastHistoryIndexInTrans).Concat(trans));
                                    }
                                }
                            }

                        };

                        GetCurrentTranscationTable(data =>
                        {
                            //目前交易紀錄如果是空的話，從資料庫取出
                            if (CurrentTranscationData.Count == 0)
                            {
                                using (ICBCDatabase db = new ICBCDatabase())
                                {
                                    //從資料庫讀取交易紀錄
                                    var transData = db.GetLastTranscations(SourceAccount.id).ToList();
                                    transData.Reverse();
                                    if (transData.Count == 0)
                                    {
                                        //資料庫沒資料的話直接開始取得紀錄
                                        StartGetTranscation(data);
                                    }
                                    else
                                    {
                                        //有資料的話進行比對
                                        var trans = transData.Select(x => x.ToTranscationData()).ToList();
                                        CompareTranscationData(data, trans);
                                    }
                                }
                            }
                            else
                            {
                                //有資料的話進行比對
                                CompareTranscationData(data, CurrentTranscationData);
                            }
                        });
                    }
                }, 5000);
            }
            else if (url.Contains("ICBCINBSReqServlet") && CurrentStatus == Status.GetTranscationDetail)
            {
                Logger.Instance.Info("Parsing transcation detail....");
                //將主交易區從iframe內複製到最外層
                IE.RunJS("$('script',$('#主交易区',$('#content-frame',$('#perbank-content-frame').contents()).contents())).remove();$('#主交易区').remove();$(document.body).append($('#主交易区',$('#content-frame',$('#perbank-content-frame').contents()).contents()).parent().html())");
                Run(() =>
                {
                    try
                    {
                        var table1 = IE.FindInputByID("主交易区");
                        var table2 = table1.FindElemetByTagName("table").FirstOrDefault();
                        var table = table2.FindElemetByTagName("table").FirstOrDefault();

                        var data = CurrentTranscationData[CurrentRowIndex];
                        data.Level = this.Level;

                        Func<IEnumerable<IHTMLElement>, IHTMLElement, string> getValue = (tds, td) =>
                        {
                            var tdlist = tds.ToList();
                            for (var i = 0; i < tdlist.Count - 1; i++)
                            {
                                if (tdlist[i] == td)
                                {
                                    return tdlist[i + 1].innerText?.Replace("&nbsp;", " ")?.Trim();
                                }
                            }

                            return null;
                        };

                        foreach (var tr in table.FindElemetByTagName("tr"))
                        {
                            var tds = tr.FindElemetByTagName("td");
                            foreach (var td in tds)
                            {
                                var titlestr = td.innerText;
                                if (!string.IsNullOrEmpty(titlestr))
                                {
                                    bool isSet = false;
                                    foreach (var title in TranscationDataMapper.Keys)
                                    {
                                        if (titlestr.Contains(title))
                                        {
                                            SetMethod(TranscationDataMapper[title], data, getValue(tds, td));
                                            isSet = true;
                                            break;
                                        }
                                    }

                                    if (!isSet)
                                    {
                                        if (titlestr.Contains("交易日期"))
                                        {
                                            var timetd = tr.FindElemetByTagName("td").ElementAt(1);
                                            var timestr = timetd.innerText.Replace("&nbsp;", " ").Trim();

                                            data.Date = DateTime.Parse(timestr);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                    }

                    CurrentStatus = Status.GetTranscation;

                    //回上一頁
                    IE.RunJS("javascript:history.go(-1);");
                }, 3000);
            }
            else if (url.Contains("ICBCINBSReqServlet") && CurrentStatus == Status.GetBatchDetail)
            {
                Logger.Instance.Info("Parsing batch detail....");
                //將主交易區從iframe內複製到最外層
                IE.RunJS("$('script',$('#主交易区',$('#content-frame',$('#perbank-content-frame').contents()).contents())).remove();$('#主交易区').remove();$(document.body).append($('#主交易区',$('#content-frame',$('#perbank-content-frame').contents()).contents()).parent().html())");
                Run(() =>
                {
                    try
                    {
                        var table1 = IE.FindInputByID("主交易区");
                        var table = table1.FindElemetByTagName("table").FirstOrDefault();

                        var data = CurrentTranscationData[CurrentRowIndex];
                        data.Level = this.Level;

                        foreach (var tr in table.FindElemetByTagName("tr"))
                        {
                            foreach (var td in tr.FindElemetByTagName("td"))
                            {
                                var tdstr = td.innerText;
                                if (!string.IsNullOrEmpty(tdstr))
                                {
                                    var tdstrs = tdstr.Split('：');
                                    if (tdstrs.Length > 1)
                                    {
                                        var titlestr = tdstrs[0];
                                        var value = tdstrs[1];
                                        foreach (var title in TranscationDataMapper.Keys)
                                        {
                                            if (tdstr.Contains(title))
                                            {
                                                Logger.Instance.Trace(titlestr + " -> found");
                                                SetMethod(TranscationDataMapper[title], data, value);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Logger.Instance.Error(e);
                    }

                    CurrentStatus = Status.GetTranscation;

                    //回上一頁
                    IE.RunJS("javascript:history.go(-1);");
                }, 3000);
            }
            else if (url.Contains("Verifyimage3") && (CurrentStatus == Status.GetBatchDetail || CurrentStatus == Status.GetTranscationDetail || CurrentStatus == Status.GetTranscation))
            {
                Logger.Instance.Info("!!! VerifyImage --> GoBack");
                Run(() =>
                {
                    //退回上一筆重新抓
                    CurrentRowIndex--;
                    CurrentStatus = Status.GetTranscation;
                    //回上一頁
                    IE.RunJS("javascript:history.go(-1);");
                }, 3000);
            }
        }

        /// <summary>
        /// 從IE畫面上取得目前的交易紀錄(清單)
        /// </summary>
        /// <param name="Callback"></param>
        private void GetCurrentTranscationTable(Action<List<TranscationData>> Callback)
        {
            Run(() =>
            {
                //將交易紀錄清單從iframe內複製到最外層
                IE.RunJS("$('.lst').remove();$(document.body).append($('.lst',$('#content-frame',$('#perbank-content-frame').contents()).contents()).parent().html());");
                Run(() =>
                {
                    var table = IE.FindElemetByClassPart("lst").FirstOrDefault();
                    var trs = table.FindElemetByTagName("tr");

                    List<TranscationData> data = new List<TranscationData>();
                    foreach (var tr in trs)
                    {
                        var tds = tr.FindElemetByTagName("td").ToList();
                        if (tds.Count > 0)
                        {
                            //var str = tds[0].innerText;
                            var d = new TranscationData()
                            {
                                Date = DateTime.Parse(tds[0].innerText),
                                Desc = tds[1].innerText,
                                Money = decimal.Parse(tds[2].innerText),
                                Balance = decimal.Parse(tds[4].innerText)
                            };
                            var a = tds[6].FindElemetByTagName("a").FirstOrDefault();
                            
                            if (a != null)
                            {
                                string href = a.getAttribute("href")?.ToString();
                                d.Goto = href.Replace("javascript:", "$('#content-frame',$('#perbank-content-frame').contents())[0].contentWindow.");
                                if (href.Contains("showHistory"))
                                {
                                    //如果是一般交易
                                    href = href.Replace("javascript:showHistory(", "").Replace("'", "").Replace(")", "");
                                    var pars = href.Split(',');
                                    d.EventID = pars[0];
                                    d.SourceAccount = pars[2];
                                    d.TargetAccount = pars[6];
                                }
                                else if (href.Contains("银联入账"))
                                {
                                    href = href.Replace("javascript:showDetail(", "").Replace("'", "").Replace(")", "");
                                    var pars = href.Split(',');
                                    d.EventID = pars[0];
                                    d.RecieverAccountEmailPhone = pars[1];
                                    d.TargetAccount = pars[5];                                    
                                    
                                    //因為可能有超過1000而造成多一個逗號，所以加入收款和餘額是否超過1000的判斷
                                    var pos = 15;
                                    if(d.Balance > 999)
                                    {
                                        pos += 1;
                                    }
                                    if(d.Money > 999)
                                    {
                                        pos += 1;
                                    }

                                    var str = HttpUtility.UrlDecode(pars[pos], System.Text.Encoding.GetEncoding("gb2312"));
                                    str = HttpUtility.UrlDecode(str, System.Text.Encoding.GetEncoding("utf-8"));
                                    d.Comment = str;
                                }
                                else
                                {
                                    d.Type = TranscationType.Batch;
                                }
                            }
                            data.Add(d);
                        }
                    }
                    Callback(data);
                }, 2000);
            });
        }

        public override void Stop()
        {
            alarm?.Stop();
            base.Stop();
        }
    }
}
