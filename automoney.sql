/*
 Navicat MySQL Data Transfer

 Source Server         : 192.168.1.143
 Source Server Type    : MySQL
 Source Server Version : 50640
 Source Host           : 192.168.1.143:48036
 Source Schema         : automoney

 Target Server Type    : MySQL
 Target Server Version : 50640
 File Encoding         : 65001

 Date: 03/05/2018 15:59:04
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '網銀帳號',
  `password` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '網銀密碼',
  `account` varchar(30) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '銀行帳號',
  `name` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '戶名',
  `bank` int(11) NULL DEFAULT NULL COMMENT '行庫代碼',
  `branches` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '分行',
  `city` varchar(10) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '城市',
  `isLogin` tinyint(4) NULL DEFAULT NULL COMMENT '1:online/ 2:offline',
  `isFetch` tinyint(4) NULL DEFAULT NULL COMMENT '1:yes/ 2:no',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for bank
-- ----------------------------
DROP TABLE IF EXISTS `bank`;
CREATE TABLE `bank`  (
  `code` int(11) NULL DEFAULT NULL,
  `bankName` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of bank
-- ----------------------------
INSERT INTO `bank` VALUES (1, '银联商务(收单)');
INSERT INTO `bank` VALUES (100, '中国邮政储蓄银行(收单)');
INSERT INTO `bank` VALUES (102, '中国工商银行');
INSERT INTO `bank` VALUES (103, '中国农业银行');
INSERT INTO `bank` VALUES (104, '中国银行');
INSERT INTO `bank` VALUES (105, '中国建设银行');
INSERT INTO `bank` VALUES (201, '国家开发银行');
INSERT INTO `bank` VALUES (202, '中国进出口银行');

-- ----------------------------
-- Table structure for transferdetail
-- ----------------------------
DROP TABLE IF EXISTS `transferdetail`;
CREATE TABLE `transferdetail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sAccountId` int(11) NULL DEFAULT NULL COMMENT '銀行帳號',
  `tAccountId` int(11) NULL DEFAULT NULL COMMENT '交易銀行帳號',
  `inOut` tinyint(4) NULL DEFAULT NULL COMMENT '1:in/ 2:out',
  `money` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '交易金額',
  `currency` varchar(4) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT 'RMB' COMMENT '幣別',
  `desc` varchar(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '摘要',
  `balance` int(11) NULL DEFAULT NULL COMMENT '餘額',
  `tranTime` datetime(0) NULL DEFAULT NULL COMMENT '交易時間',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_unicode_ci ROW_FORMAT = Compact;

SET FOREIGN_KEY_CHECKS = 1;
